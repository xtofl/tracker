#!/usr/bin/env python3

import flask
from collections import Counter

app = flask.Flask(__name__)

trackers = Counter()
from logging import *
getLogger().setLevel(DEBUG)

@app.route("/")
def list_trackers():
    return dict(trackers)

@app.route("/reset")
def reset_trackers():
    trackers = Counter()
    return dict(trackers)

@app.route("/<string:tracker>")
def tracker_get(tracker):
    return {tracker: trackers[tracker]}

@app.route("/<string:tracker>/dec")
def tracker_decrease(tracker):
    trackers[tracker] -= 1
    return tracker_get(tracker)

@app.route("/<string:tracker>/inc")
def tracker_increase(tracker):
    trackers[tracker] += 1
    return tracker_get(tracker)

if __name__ == "__main__":
    app.run(host="0.0.0.0")
