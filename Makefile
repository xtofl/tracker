

uninstall:
	systemctl disable tracker.service
	rm /etc/systemd/system/tracker.service

install:
	cp tracker.service /etc/systemd/system/tracker.service
	systemctl enable tracker.service
